package com.example.myteacher.views.importTranslations.chooseLessonDialog

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.lesson.lesson.LessonModel

class ChooseLessonViewModel(application: Application): AndroidViewModel(application) {
    private val model =
        LessonModel(
            Database.getInstance(application).lessonDao
        )
    val allLessons = model.allLessons
}