package com.example.myteacher.views.overview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myteacher.databinding.OverviewLessonListItemBinding
import com.example.myteacher.dataClasses.lesson.lesson.Lesson

class OverviewLessonRecyclerViewAdapter(private val clickListener: LessonClickListener): ListAdapter<Lesson,
        OverviewLessonRecyclerViewAdapter.ViewHolder>(LessonDifCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(clickListener, getItem(position))
    }


    class ViewHolder private constructor(val binding: OverviewLessonListItemBinding)
        : RecyclerView.ViewHolder(binding.root){

        fun bind(clickListener: LessonClickListener, item: Lesson){
            binding.lesson = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = OverviewLessonListItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }

    }


}

class LessonDifCallback: DiffUtil.ItemCallback<Lesson>(){
    override fun areItemsTheSame(oldItem: Lesson, newItem: Lesson): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Lesson, newItem: Lesson): Boolean {
        return oldItem == newItem
    }

}

class LessonClickListener(val clickListener: (id: Long) -> Unit){
    fun onClick(lesson: Lesson) = clickListener(lesson.id)
}

