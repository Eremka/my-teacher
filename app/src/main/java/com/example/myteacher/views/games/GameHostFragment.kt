package com.example.myteacher.views.games

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentGameHostBinding

class GameHostFragment: Fragment() {

    private val args: GameHostFragmentArgs by navArgs()
    private lateinit var viewModel: GameViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentGameHostBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_game_host, container, false)

        val lessonId = args.lessonId
        val application = requireNotNull(this.activity).application
        val viewModelFactory = GameViewModelFactory(lessonId, application)

        viewModel = ViewModelProviders.of(this.activity!!, viewModelFactory)[GameViewModel::class.java]

        viewModel.exitGame.observe(this, Observer {
            if (it){
                this.findNavController().navigate(R.id.action_gameHostFragment_to_overviewFragment)
                viewModel.onContinueButtonClickedComplete()
            }
        })

        viewModel.setTimer(binding.gameHostTimeChronometer)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    fun pauseTimer(){
        viewModel.pauseTimer()
    }

    fun continueTimer(){
        viewModel.continueTimer()
    }

    fun finishGame(){
        viewModel.finishGame()
    }

    override fun onDestroy() {
        /**
         * Clearing the ViewModels attached to the activity, as they are no longer needed. Clearing
         * needs to be done as they are shared and thus not attached to this fragment, but rather
         * to the main activity. No other ViewModels are expected to be active on this activity.
         */
        activity?.viewModelStore?.clear()
        super.onDestroy()
    }
}