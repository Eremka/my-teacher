package com.example.myteacher.views.importTranslations

import android.Manifest
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentImportTranslationsBinding
import com.example.myteacher.views.importTranslations.chooseLessonDialog.ChooseLessonDialog
import timber.log.Timber
import java.io.File

const val REQUEST_PERMISSIONS = 123

class ImportTranslationsFragment : Fragment(), ChooseLessonDialog.ChooseLessonListener {

    private lateinit var viewModel: ImportTranslationsViewModel
    private lateinit var dialog: ChooseLessonDialog
    private val args: ImportTranslationsFragmentArgs by navArgs()
    private var dir: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentImportTranslationsBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_import_translations, container, false
        )

        val databaseId = args.databaseId
        dir = args.fileDirectory

        if (dir != null) {
            val file = File(dir)
            val activity = requireNotNull(this.activity) as AppCompatActivity
            activity.supportActionBar?.title = file.name
        }

        viewModel = ViewModelProviders.of(this)[ImportTranslationsViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.lessonId = databaseId

        val adapter = ImportTranslationsRecyclerViewAdapter(FileClickListener {
            viewModel.onFileClick(it)
        })
        binding.importTranslationsRecyclerView.adapter = adapter

        viewModel.listOfDirectories.observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                adapter.submitList(it.asList())
            }
        })

        viewModel.goToNextFile.observe(this, Observer {
            if (it != null) {
                val action =
                    ImportTranslationsFragmentDirections.actionImportTranslationsFragmentSelf(
                        databaseId,
                        it
                    )
                findNavController().navigate(action)
                viewModel.onFileClickComplete()
            }
        })

        viewModel.openChooseLessonDialog.observe(this, Observer {
            if (databaseId == -1L) {
                dialog = ChooseLessonDialog()
                dialog.setTargetFragment(this, 123)
                fragmentManager?.let { manager ->
                    dialog.show(manager, "ChooseLessonDialog")
                }

                dialog.dialog?.setOnDismissListener(DialogInterface.OnDismissListener { _ ->
                    viewModel.startLoadingFile(it)
                    Timber.d("Dialog dismissed")
                })
            } else {
                viewModel.startLoadingFile(it)
            }
        })

        viewModel.filePathForLoading.observe(this, Observer {
            Timber.d("The file path for loading is: $it")
            if (it != null) {
                val action =
                    ImportTranslationsFragmentDirections.actionImportTranslationsFragmentToImportTranslationsComplete(
                        it, viewModel.lessonId
                    )
                findNavController().navigate(action)
                viewModel.onFilePathForLoadingComplete()
            }
        })

        return binding.root
    }

    override fun onResume() {
        if (ContextCompat.checkSelfPermission(
                this.context!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            getPermissions()
        } else {
            viewModel.setDirectory(dir?.let { File(dir) })
        }
        super.onResume()
    }

    private fun getPermissions() {
        val activity = requireNotNull(this.activity)
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {

        } else {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_PERMISSIONS
            )
        }
    }

    /**
     * When the permission is not granted, the user will be directed back to the previous activity
     * as no import can take place.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSIONS -> {
                if ((grantResults.isNotEmpty() || grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    viewModel.setDirectory(File(dir))
                } else {
                    permissionDenied()
                }
            }
            else -> permissionDenied()
        }

    }

    private fun permissionDenied() {
        Toast.makeText(
            this.context, getString(R.string.must_grant_permissions),
            Toast.LENGTH_SHORT
        ).show()
        activity?.onBackPressed()
    }

    override fun onLessonClicked(id: Long) {
        Timber.d("Lesson id in ImportTranslationsFragment: $id")
        viewModel.lessonId = id
        dialog.dismiss()
    }
}