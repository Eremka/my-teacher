package com.example.myteacher.views.translationList

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentTranslationListBinding

class TranslationListFragment: Fragment() {

    private lateinit var viewModel: TranslationListViewModel
    private val args: TranslationListFragmentArgs by navArgs()

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.translation_list_import_translations_menu -> {
                viewModel.goToImportTranslations()
                true
            }
            R.id.translation_list_add_translation_menu -> {
                viewModel.goToCreateTranslation()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.translation_list_menu, menu)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentTranslationListBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_translation_list, container, false)

        setHasOptionsMenu(true)

        val application = requireNotNull(this.activity).application

        val lessonId = args.databaseId

        val viewModelFactory = TranslationListViewModelFactory(lessonId, application)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[TranslationListViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val adapter = TranslationListRecyclerViewAdapter()
        binding.translationListRecyclerView.adapter = adapter

        viewModel.allTranslationsInDatabase.observe(this, Observer {
            adapter.submitList(it)
        })

        viewModel.goToCreateTranslation.observe(this, Observer {
            if (it){
                val action = TranslationListFragmentDirections.actionTranslationListFragmentToCreateTranslationFragment(lessonId)
                findNavController().navigate(action)
                viewModel.goToCreateTranslationComplete()
            }
        })

        viewModel.goToImportTranslations.observe(this, Observer {
            if (it){
                val action = TranslationListFragmentDirections.actionTranslationListFragmentToImportTranslationsFragment(lessonId, null)
                findNavController().navigate(action)
                viewModel.goToImportTranslationsComplete()
            }
        })

        return binding.root
    }
}