package com.example.myteacher.views.games

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.myteacher.R

class FinishGameDialog: DialogFragment() {

    internal lateinit var listener: FinishGameListener

    interface FinishGameListener{
        fun onFinishGame(dialog: DialogFragment)
        fun onNotFinishGame(dialog: DialogFragment)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        try {
            listener = context as FinishGameListener
        } catch (e: ClassCastException) {
            throw ClassCastException((context.toString() +
                    " must implement FinishGameListener"))
        }

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle("End game?")
            builder.setMessage("Are you sure you are done learning?")
            builder.setPositiveButton(getString(R.string.yes), DialogInterface.OnClickListener{ _, _ ->
                listener.onFinishGame(this)
            })
            builder.setNegativeButton(getString(R.string.no), DialogInterface.OnClickListener{ _, _ ->
                listener.onNotFinishGame(this)
            })
            return builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}