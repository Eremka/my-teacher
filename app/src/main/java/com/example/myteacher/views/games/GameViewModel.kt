package com.example.myteacher.views.games

import android.app.Application
import android.os.SystemClock
import android.widget.Chronometer
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.lesson.completedLesson.CompletedLesson
import com.example.myteacher.dataClasses.lesson.completedLesson.CompletedLessonModel
import com.example.myteacher.dataClasses.translation.answer.Answer
import com.example.myteacher.dataClasses.translation.TranslationModel
import com.example.myteacher.dataClasses.translation.answer.AnswerModel
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslation
import kotlinx.coroutines.launch
import timber.log.Timber
import java.security.SecureRandom
import java.util.*
import kotlin.collections.ArrayList

class GameViewModel(val databaseId: Long, application: Application) : AndroidViewModel(application) {

    private val completedLessonModel = CompletedLessonModel(Database.getInstance(
                application).completedLessonDao)

    private val answerModel = AnswerModel(
            Database.getInstance(application).answerDao)

    private var index = -1
    private lateinit var timer: Chronometer
    private var timeWhenStopped = 0L
    private lateinit var translationList: MutableList<WordTranslation>
    private var completedLesson = CompletedLesson(
        lessonId = 0L,
        timeStarted = 0L
    )

    var gameType: GameType = GameType.NO_GAME
    private val translationModel: TranslationModel =
        TranslationModel(Database.getInstance(getApplication()).wordTranslationDao)

    private val answerList = ArrayList<Answer>()

    private val _nrCorrect = MutableLiveData(0)
    val nrCorrect: LiveData<Int>
        get() = _nrCorrect

    private val _nrWrong = MutableLiveData(0)
    val nrWrong: LiveData<Int>
        get() = _nrWrong

    private val _currentTranslation = MutableLiveData<WordTranslation>()
    val currentTranslation: LiveData<WordTranslation>
        get() = _currentTranslation

    private val _startDifficultGame = MutableLiveData<Boolean>()
    val startDifficultGame: LiveData<Boolean>
        get() = _startDifficultGame

    private val _gameFinished = MutableLiveData<Boolean>()
    val gameFinished: LiveData<Boolean>
        get() = _gameFinished

    private val _gameStarted = MutableLiveData<Boolean>()
    val gameStarted: LiveData<Boolean>
        get() = _gameStarted

    private val _showAnswer = MutableLiveData<Boolean>(false)
    val showAnswer: LiveData<Boolean>
        get() = _showAnswer

    private val _exitGame = MutableLiveData<Boolean>()
    val exitGame: LiveData<Boolean>
        get() = _exitGame

    init {
        viewModelScope.launch {
            getAllTranslations()
        }
    }

    private suspend fun getAllTranslations() {
        translationList = translationModel.getAllWordTranslationsAsList(databaseId)!!.toMutableList()
    }

    private fun getNewTranslation() {
        if (translationList.isNotEmpty()) {
            val size = translationList.size
            index = getRandomInt(size)
            newGame(translationList[index])
        } else {
           finishGame()
        }
    }

    private fun getRandomInt(maxSize: Int): Int {
        return SecureRandom().nextInt(maxSize)
    }

    fun gameFinishedComplete(){
        _gameFinished.value = false
    }

    fun finishGame(){
        timer.stop()

        viewModelScope.launch {
            if (_gameStarted.value != null && _gameStarted.value!!) {
                completedLesson.duration = timer.text.toString()
                completedLessonModel.updateLesson(completedLesson)

                _gameFinished.value = true
                _gameStarted.value = false
            }
        }
    }

    fun pauseTimer(){
        timeWhenStopped = timer.base - SystemClock.elapsedRealtime()
        timer.stop()
    }

    fun continueTimer(){
        timer.base = SystemClock.elapsedRealtime() + timeWhenStopped
        timer.start()
    }

    fun startGame() {
        viewModelScope.launch {
            completedLesson.timeStarted = System.currentTimeMillis()
            val completedLessonId = completedLessonModel.insertLesson(completedLesson)
            completedLesson.id = completedLessonId

            _gameStarted.value = true
            getNewTranslation()

            completedLesson.lessonId = currentTranslation.value!!.databaseId
        }
    }

    fun startFiveMinGame() {
        _gameStarted.value = true
        startGame()
    }

    fun startDifficultGame() {
        _gameStarted.value = true
        timer.start()
        startGame()
    }

    private fun newGame(translation: WordTranslation) {
        Timber.d("New game")
        when {
            translation.levelOne < 2 -> {
                // TODO remove and substitute for starting an easy game
                gameType = GameType.DIFFICULT
                _startDifficultGame.value = true
            }
            translation.levelTwo < 2 -> {
                // TODO remove and substitute for starting an medium game
                gameType = GameType.DIFFICULT
                _startDifficultGame.value = true
            }
            else -> {
                gameType = GameType.DIFFICULT
                _startDifficultGame.value = true
            }
        }

        _currentTranslation.value = translation
        Timber.d(translation.toString())
    }

    fun gameStarted() {
        _startDifficultGame.value = false
    }

    private fun answerCorrect() {
        translationList.removeAt(index)

        _nrCorrect.value?.inc()

        viewModelScope.launch {
            if (answerModel.getExistingAnswerCount(currentTranslation.value!!.id, completedLesson.id) == 0) {
                when (gameType) {
                    GameType.DIFFICULT -> currentTranslation.value!!.levelThree += 1
                    GameType.MEDIUM_L1_TO_L2, GameType.MEDIUM_L2_TO_L1 ->
                        currentTranslation.value!!.levelTwo += 1
                    else -> currentTranslation.value!!.levelOne += 1
                }
            }

            val newlyLearned: Boolean =
                currentTranslation.value!!.levelThree == 2
                        && gameType == GameType.DIFFICULT
                        && answerModel.getExistingAnswerCount(currentTranslation.value!!.id, completedLesson.id) == 0

            if (newlyLearned){
                completedLesson.newWordsLearned += 1
            }

            val answer = Answer(
                translationId = currentTranslation.value!!.id,
                completedLessonId = completedLesson.id,
                isCorrect = true,
                time = System.currentTimeMillis(),
                isNewlyLearned = newlyLearned
            )

            answerList.add(answer)
            answerModel.insertAnswer(answer)
            Timber.d(answer.toString())

            completedLesson.numberAttempt += 1
            completedLesson.numberCorrect += 1

            _currentTranslation.value!!.correct += 1
            _currentTranslation.value!!.attempts += 1

            translationModel.updateTranslation(currentTranslation.value!!)
            Timber.d(currentTranslation.value.toString())
            getNewTranslation()
        }
    }

    private fun answerWrong() {
        completedLesson.numberAttempt += 1
        _showAnswer.value = true
        _nrWrong.value?.inc()

        viewModelScope.launch {
            val answer = Answer(
                completedLessonId = completedLesson.id,
                translationId = currentTranslation.value!!.id,
                isCorrect = false,
                time = System.currentTimeMillis()
            )

            answerList.add(answer)
            answerModel.insertAnswer(answer)

            currentTranslation.value!!.attempts += 1
            translationModel.updateTranslation(currentTranslation.value!!)
        }
    }

    fun onTextChanged(p0: CharSequence?, p1: Int, p2:Int, p3: Int) {
        if (p0 != null && p0.toString().toLowerCase(Locale.getDefault())
                .equals(currentTranslation.value?.language1?.toLowerCase(Locale.getDefault()))) {
            answerCorrect()
        }
    }

    fun setTimer(t: Chronometer){
        this.timer = t
    }

    fun onContinueButtonClicked(){
        _exitGame.value = true
    }

    fun onContinueButtonClickedComplete(){
        _gameFinished.value = false
        _exitGame.value = false
    }

    /**
     * This is the code that handles the I dont know/Next button. When the button is clicked the first
     * time, the answer is shown. When it is clicked the second time, a new translation will be loaded
     * from the list.
     */
    fun onIdkButtonClicked(){
       if (_showAnswer.value == true){
           getNewTranslation()
           _showAnswer.value = false
       } else {
           answerWrong()
           _showAnswer.value = true
       }
    }
}