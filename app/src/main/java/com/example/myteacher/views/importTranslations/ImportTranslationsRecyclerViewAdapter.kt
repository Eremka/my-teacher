package com.example.myteacher.views.importTranslations

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myteacher.databinding.ImportTranlsationsListItemBinding
import com.example.myteacher.databinding.OverviewLessonListItemBinding
import java.io.File

class ImportTranslationsRecyclerViewAdapter(private val clickListener: FileClickListener): ListAdapter<File,
        ImportTranslationsRecyclerViewAdapter.ViewHolder>(FileDifCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(clickListener, getItem(position))
    }


    class ViewHolder private constructor(val binding: ImportTranlsationsListItemBinding)
        : RecyclerView.ViewHolder(binding.root){

        fun bind(clickListener: FileClickListener, item: File){
            binding.file = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ImportTranlsationsListItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }

}

class FileDifCallback: DiffUtil.ItemCallback<File>(){
    override fun areItemsTheSame(oldItem: File, newItem: File): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: File, newItem: File): Boolean {
        return oldItem.name == newItem.name
    }
}

class FileClickListener(val clickListener: (file: File) -> Unit){
    fun onClick(file: File) = clickListener(file)
}
