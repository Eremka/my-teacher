package com.example.myteacher.views.translationList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myteacher.databinding.OverviewLessonListItemBinding
import com.example.myteacher.databinding.WordTranslationListItemBinding
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslation

class TranslationListRecyclerViewAdapter(): ListAdapter<WordTranslation,
        TranslationListRecyclerViewAdapter.ViewHolder>(WordTranslationDifCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder private constructor(val binding: WordTranslationListItemBinding)
        : RecyclerView.ViewHolder(binding.root){

        fun bind(item: WordTranslation){
            binding.translation = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = WordTranslationListItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }

    }
}

class WordTranslationDifCallback: DiffUtil.ItemCallback<WordTranslation>(){
    override fun areItemsTheSame(oldItem: WordTranslation, newItem: WordTranslation): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: WordTranslation, newItem: WordTranslation): Boolean {
        return oldItem == newItem
    }

}