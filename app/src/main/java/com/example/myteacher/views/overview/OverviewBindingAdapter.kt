package com.example.myteacher.views.overview

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import com.example.myteacher.R

class OverviewBindingAdapter {

    companion object{
        @BindingAdapter("getLanguageFlag")
        @JvmStatic fun getLanguageFlag(view: ImageView, language: String){
            when (language){
                view.context.getString(R.string.slovak) -> view.setImageDrawable(view.context.getDrawable(R.drawable.ic_slovakia_flag_48dp))
                view.context.getString(R.string.czech) -> view.setImageDrawable(view.context.getDrawable(R.drawable.ic_czech_republic_flag_48dp))
                view.context.getString(R.string.english) -> view.setImageDrawable(view.context.getDrawable(R.drawable.ic_united_kingdom_flag_48dp))
                view.context.getString(R.string.dutch) -> view.setImageDrawable(view.context.getDrawable(R.drawable.ic_netherlands_flag_48dp))
                view.context.getString(R.string.german) -> view.setImageDrawable(view.context.getDrawable(R.drawable.ic_germany_flag_48dp))
            }
        }

        @BindingAdapter("getProgress")
        @JvmStatic fun getProgress(view: ProgressBar, progress: Double){
            view.progress = progress.toInt()
        }

        @BindingAdapter("getAwardImage")
        @JvmStatic fun getAward(view: ImageView, progress: Double){
            if (progress == 100.0){
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
        }
    }
}