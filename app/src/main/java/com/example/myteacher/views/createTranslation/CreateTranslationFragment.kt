package com.example.myteacher.views.createTranslation

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentCreateTranslationBinding
import com.google.android.material.snackbar.Snackbar

class CreateTranslationFragment: Fragment() {

    private lateinit var viewModel: CreateTranslationViewModel
    private val args: CreateTranslationFragmentArgs by navArgs()


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.saveLessonsMenu -> {
                viewModel.saveTranslation()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.create_menu, menu)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentCreateTranslationBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_create_translation, container, false)

        setHasOptionsMenu(true)

        val application = requireNotNull(this.activity).application

        val lessonId = args.databaseId

        val viewModelFactory = CreateTranslationViewModelFactory(lessonId, application)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[CreateTranslationViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.lesson.observe(this, Observer {
            binding.lesson = it
        })

        viewModel.goToCreateTranslation.observe(this, Observer {
            if (it) {
                val action =
                    CreateTranslationFragmentDirections.actionCreateTranslationFragmentSelf(lessonId)
                findNavController().navigate(action)
                viewModel.goToCreateTranslationComplete()
            }
        })

        viewModel.showSnackBar.observe(this, Observer {
            if (it > 0){
                Snackbar.make(binding.root, getString(R.string.translation_saved, it), Snackbar.LENGTH_SHORT).show()
                viewModel.showSnackBarComplete()
            }
        })

        return binding.root
    }
}