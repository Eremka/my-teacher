package com.example.myteacher.views.games.startNewGame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentStartNewGameBinding
import com.example.myteacher.views.games.GameViewModel

class StartNewGameFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val binding: FragmentStartNewGameBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_start_new_game, container, false)

        val viewModel = ViewModelProviders.of(activity!!)[GameViewModel::class.java]
        binding.viewModel = viewModel

        viewModel.startDifficultGame.observe(this, Observer {
            if (it){
                viewModel.gameStarted()
                this.findNavController().navigate(R.id.action_startNewGameFragment_to_difficultGameOne)
            }
        })

        return binding.root
    }
}