package com.example.myteacher.views.importTranslations.importTranslationsComplete

import com.example.myteacher.dataClasses.lesson.lesson.Lesson
import com.example.myteacher.dataClasses.translation.ImportedTranslation
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslation
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import timber.log.Timber
import java.io.File

fun importTranslationsXLSX(file: File, databaseId: Long): ImportedTranslation{
    Timber.d("File path is $file and database id is: $databaseId")

    val importedTranslation = ImportedTranslation(Lesson(), ArrayList())
    val workbook = XSSFWorkbook(file)
    val sheet = workbook.getSheetAt(1)
    val iterator = sheet.iterator()

    Timber.d(sheet.toString())
    Timber.d(sheet.topRow.toString())

    while (iterator.hasNext()){
        val row = iterator.next()
        Timber.d(row.toString())

        val translation = WordTranslation(databaseId)
        translation.language1 = row.getCell(0).stringCellValue.trim()
        translation.language2 = row.getCell(1).stringCellValue.trim()
        translation.exampleSentence = row.getCell(2).stringCellValue.trim()
        translation.exampleSentenceAnswer = row.getCell(3).stringCellValue.trim()

        importedTranslation.translationList.add(translation)
    }

    return importedTranslation
}