package com.example.myteacher.views.importTranslations.importTranslationsComplete

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs

import com.example.myteacher.R
import com.example.myteacher.databinding.ImportTranslationsCompleteFragmentBinding
import com.example.myteacher.views.importTranslations.ImportTranslationsFragmentArgs
import timber.log.Timber

class ImportTranslationsComplete : Fragment() {

    private val args: ImportTranslationsCompleteArgs by navArgs()
    private lateinit var viewModel: ImportTranslationsCompleteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val binding: ImportTranslationsCompleteFragmentBinding = DataBindingUtil.inflate(inflater,
            R.layout.import_translations_complete_fragment, container, false)

        val path = args.filePath
        val databaseId = args.databaseId
        val application = requireNotNull(this.activity).application
        val viewModelFactory = ImportTranslationsCompleteViewModelFactory(path, databaseId, application)
        Timber.d("The path is $path and the databaseId is $databaseId")

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ImportTranslationsCompleteViewModel::class.java]
        binding.viewModel = viewModel

        viewModel.goBackToTranslationList.observe(this, Observer {
            if (it){
                findNavController().navigateUp()
                viewModel.goBackToTranslationListComplete()
            }
        })

        viewModel.numberOfTranslationsImported.observe(this, Observer {
            if (it != null && it == 0){
                binding.importCompleteMessageTv.text = getString(R.string.import_unsuccessful)
                binding.importCompleteNrOfImportsTv.text = getString(R.string.number_plus_imports, it)

                binding.importCompleteProgressBar.visibility = View.GONE
                binding.importCompleteNrOfImportsTv.visibility = View.VISIBLE
                binding.importCompleteMessageTv.visibility = View.VISIBLE
                binding.importCompleteContinueButton.visibility = View.VISIBLE
            } else if (it != null){
                binding.importCompleteMessageTv.text = getString(R.string.import_successful)
                binding.importCompleteNrOfImportsTv.text = getString(R.string.number_plus_imports, it)

                binding.importCompleteProgressBar.visibility = View.GONE
                binding.importCompleteNrOfImportsTv.visibility = View.VISIBLE
                binding.importCompleteMessageTv.visibility = View.VISIBLE
                binding.importCompleteContinueButton.visibility = View.VISIBLE
            }
        })

        return binding.root
    }
}
