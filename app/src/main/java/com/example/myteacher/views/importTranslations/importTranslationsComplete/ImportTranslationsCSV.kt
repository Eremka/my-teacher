package com.example.myteacher.views.importTranslations.importTranslationsComplete

import com.example.myteacher.dataClasses.lesson.lesson.Lesson
import com.example.myteacher.dataClasses.translation.ImportedTranslation
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslation
import org.apache.commons.csv.CSVFormat
import java.io.File
import java.io.FileReader

@Throws(IllegalArgumentException::class)
fun importTranslationsCSV(file: File, databaseId: Long): ImportedTranslation{
    val records = CSVFormat.newFormat(';').withFirstRecordAsHeader().parse(FileReader(file))

    val importedTranslation = ImportedTranslation(Lesson(id = databaseId), ArrayList())
    for (record in records){

        val translation = WordTranslation(databaseId)
        translation.language1 = record.get(0).trim()
        translation.language2 = record.get(1).trim()
        translation.exampleSentence = record.get(2).trim()
        translation.exampleSentenceAnswer = record.get(3).trim()
        importedTranslation.translationList.add(translation)
    }

    return importedTranslation
}

