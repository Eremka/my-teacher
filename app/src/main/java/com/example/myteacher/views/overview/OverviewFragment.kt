package com.example.myteacher.views.overview

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentOverviewBinding
import timber.log.Timber

class OverviewFragment: Fragment() {

    companion object{
        const val ON_CLICKED_ID = "id"
    }

    private lateinit var viewModel: OverviewViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentOverviewBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_overview, container, false)

        viewModel = ViewModelProviders.of(this)[OverviewViewModel::class.java]

        val adapter = OverviewLessonRecyclerViewAdapter(LessonClickListener { id ->
            val bundle = Bundle()
            bundle.putLong(ON_CLICKED_ID, id)
            showOptionDialog(bundle).show()
        })

        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.overviewRecyclerView.adapter = adapter
        binding.overviewFab.isVisible = true

        viewModel.allLessons.observe(this, Observer {
            adapter.submitList(it)
        })

        viewModel.navigateToAddLesson.observe(this, Observer {
            if (it){
                this.findNavController().navigate(R.id.action_overviewFragment_to_createLessonFragment)
                viewModel.navigateToCreateBookComplete()
            }
        })

        viewModel.navigateToTranslationsList.observe(this, Observer {
            if (it >= 0L){
                val action = OverviewFragmentDirections.actionOverviewFragmentToTranslationListFragment(it)
                findNavController().navigate(action)
                viewModel.navigateToTranslationsListComplete()
            }
        })

        viewModel.navigateToLearningGames.observe(this, Observer {
            if (it >= 0L){
                val action = OverviewFragmentDirections.actionOverviewFragmentToGameHostFragment(it)
                findNavController().navigate(action)
                viewModel.navigateToLearningGamesComplete()
            }
        })

        viewModel.navigateToImportTranslations.observe(this, Observer {
            if (it > -1L){
                val action = OverviewFragmentDirections.actionOverviewFragmentToImportTranslationsFragment(it, null)
                findNavController().navigate(action)
                viewModel.navigateToImportTranslationsComplete()
            }
        })

        return binding.root
    }

    private fun showOptionDialog(savedInstanceState: Bundle?): Dialog{
        val id = savedInstanceState?.getLong(ON_CLICKED_ID)
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(R.string.what_to_do_overview_options)
                .setItems(R.array.overview_options,
                    DialogInterface.OnClickListener { _ , which ->
                        when (which){
                            0 -> viewModel.navigateToLearningGames(id!!) // Learn
                            1 -> viewModel.navigateToTranslationsList(id!!) // Create new translation
                            2 -> viewModel.navigateToImportTranslations(id!!) //Import translations from file
                            else -> Timber.d("This option is not available")
                        }
                    })
            builder.create()
        }?: throw IllegalStateException("Activity cannot be null")
    }
}