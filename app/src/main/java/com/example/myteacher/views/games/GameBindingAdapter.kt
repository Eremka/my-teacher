package com.example.myteacher.views.games

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.databinding.ObservableField
import androidx.databinding.adapters.ListenerUtil
import androidx.databinding.adapters.TextViewBindingAdapter
import com.example.myteacher.R
import timber.log.Timber


class GameBindingAdapter {

    companion object{
        @BindingAdapter(value = ["hintVisibilityGameStarted", "hintVisibilityGameType", "hintVisibilityHint"], requireAll = true)
        @JvmStatic fun setHintVisibility(view: Button, gameStarted: Boolean, gameType: GameType, hint: String?){
            if (gameStarted){
                if (hint != null && (gameType == GameType.EASY_L1_TO_L2 || gameType == GameType.MEDIUM_L1_TO_L2)){
                    view.visibility = View.VISIBLE
                } else {
                    view.visibility = View.GONE
                }
            } else{
                view.visibility = View.INVISIBLE
            }
        }

        @BindingAdapter("answerViewVisibility")
        @JvmStatic fun answerViewVisibility(view: TextView, message: String?){
            if (message == null){
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
                view.text = message
            }
        }
    }
}