package com.example.myteacher.views.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentLoginBinding
import com.firebase.ui.auth.AuthUI
import timber.log.Timber
import java.lang.Exception

private const val LOGIN_REQUEST = 100

class LoginFragment : Fragment() {

    private val providers = arrayListOf(
        AuthUI.IdpConfig.EmailBuilder().build(),
        AuthUI.IdpConfig.GoogleBuilder().build()
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val binding : FragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login,
            container, false)

        val loginViewModel = activity?.run {
            ViewModelProviders.of(this)[LoginViewModel::class.java]
        }?: throw Exception("Invalid class")

        binding.viewModel = loginViewModel
        binding.lifecycleOwner = this

        /**
         * it == false means that the user is logged in and the app should navigate to the
         * next screen.
         */
        loginViewModel.isUserSignedIn.observe(this, Observer {
            if (it){
                navigateToOverviewFragment()
            } else {
                startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .build(), LOGIN_REQUEST
                )
            }
        })

        return binding.root
    }

    private fun navigateToOverviewFragment() {
        this.findNavController().navigate(R.id.action_loginFragment_to_overviewFragment)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LOGIN_REQUEST && resultCode == Activity.RESULT_OK){
            navigateToOverviewFragment()
            Timber.d("Logged in successfully")
        } else {
            Toast.makeText(this.context, "Something has gone wrong with logging in", Toast.LENGTH_SHORT).show()
        }
    }
}