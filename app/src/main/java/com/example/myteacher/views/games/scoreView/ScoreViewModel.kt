package com.example.myteacher.views.games.scoreView

import android.app.Application
import androidx.lifecycle.*
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.lesson.completedLesson.CompletedLesson
import com.example.myteacher.dataClasses.lesson.completedLesson.CompletedLessonModel
import com.example.myteacher.dataClasses.translation.TranslationModel
import com.example.myteacher.dataClasses.translation.answer.AnswerModel
import kotlinx.coroutines.launch
import timber.log.Timber

class ScoreViewModel(application: Application) : AndroidViewModel(application) {

    private val _lastLesson = MutableLiveData<CompletedLesson>()
    val lastLesson: LiveData<CompletedLesson>
        get() = _lastLesson

    private val _secondToLastLesson = MutableLiveData<CompletedLesson>()
    private val secondToLastLesson: LiveData<CompletedLesson>
        get() = _secondToLastLesson

    private val _wordsCorrectAtFirstTry = MutableLiveData<Int>()
    val wordsCorrectAtFirstTry: LiveData<Int>
        get() = _wordsCorrectAtFirstTry

    private val _lessonComplete = MutableLiveData<Double>()
    val lessonComplete: LiveData<Double>
        get() = _lessonComplete

    private val completedLessonModel = CompletedLessonModel(
        Database.getInstance(application).completedLessonDao)

    private val answerModel = AnswerModel(
        Database.getInstance(application).answerDao)

    private val translationModel = TranslationModel(
        Database.getInstance(application).wordTranslationDao)

    private val _numberOfWordsInLesson = MutableLiveData<Int>()
    val numberOfWordsInLesson: LiveData<Int>
        get() = _numberOfWordsInLesson

        init {
        viewModelScope.launch {
            val lastTwoCompletedLessons = completedLessonModel.getLastTwoCompletedLessons()
            if (lastTwoCompletedLessons.size == 2){
                _secondToLastLesson.value = lastTwoCompletedLessons[1]
            }

            _lastLesson.value = lastTwoCompletedLessons[0]

            if (lastLesson.value!!.improvementPercentage == 0.0 && secondToLastLesson.value != null) {
                val secondToLastCorrect =
                    secondToLastLesson.value!!.numberCorrect.toDouble() / secondToLastLesson.value!!.numberAttempt.toDouble()
                val lastCorrect =
                    lastLesson.value!!.numberCorrect.toDouble() / lastLesson.value!!.numberAttempt.toDouble()

                if (lastCorrect > secondToLastCorrect) {
                    _lastLesson.value!!.improvementPercentage = (lastCorrect - secondToLastCorrect) * 100
                }

                completedLessonModel.updateLesson(lastLesson.value!!)
            }

            getLessonComplete(lastLesson.value!!.lessonId)
            getWordsCorrectAtFirstTry(lastLesson.value!!.id)
            getNumberOfWordsInLesson(lastLesson.value!!.lessonId)
        }
    }

    private suspend fun getWordsCorrectAtFirstTry(completedLessonId: Long){
        _wordsCorrectAtFirstTry.value = answerModel.getAnswersCorrectAtFirstTry(completedLessonId)
    }

    private suspend fun getLessonComplete(lessonId: Long){
        _lessonComplete.value = translationModel.getPercentageTranslationsLearned(lessonId)
    }

    private suspend fun getNumberOfWordsInLesson(lessonId: Long){
        _numberOfWordsInLesson.value = translationModel.getNumberOfTranslationsInLesson(lessonId)
    }
}
