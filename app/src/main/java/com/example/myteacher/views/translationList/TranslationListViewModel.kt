package com.example.myteacher.views.translationList

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.translation.TranslationModel

class TranslationListViewModel(
    lessonId: Long,
    application: Application
) : AndroidViewModel(application) {

    //private val lessonSource = LessonModel(Database.getInstance(application).lessonDao)
    private val wordTranslationSource =
        TranslationModel(Database.getInstance(application).wordTranslationDao)

    val allTranslationsInDatabase = wordTranslationSource.getAllWordTranslationsInDatabase(lessonId)
    val numberOfTranslationsInDatabase = wordTranslationSource.getNumberOfTranslationsInLessonLiveData(lessonId)

    private val _goToImportTranslations = MutableLiveData<Boolean>()
    val goToImportTranslations: LiveData<Boolean>
        get() = _goToImportTranslations

    fun goToImportTranslations() {
        _goToImportTranslations.value = true
    }

    fun goToImportTranslationsComplete() {
        _goToImportTranslations.value = false
    }

    private val _goToCreateTranslation = MutableLiveData<Boolean>()
    val goToCreateTranslation: LiveData<Boolean>
        get() = _goToCreateTranslation

    fun goToCreateTranslation(){
        _goToCreateTranslation.value = true
    }

    fun goToCreateTranslationComplete(){
        _goToCreateTranslation.value = false
    }

}