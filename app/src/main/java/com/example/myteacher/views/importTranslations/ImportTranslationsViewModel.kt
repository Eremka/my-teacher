package com.example.myteacher.views.importTranslations

import android.app.Application
import android.os.Environment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.myteacher.R
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.lesson.lesson.LessonModel
import com.example.myteacher.dataClasses.translation.ImportedTranslation
import com.example.myteacher.dataClasses.translation.TranslationModel
import com.example.myteacher.views.importTranslations.importTranslationsComplete.importTranslationsCSV
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File

class ImportTranslationsViewModel(application: Application) : AndroidViewModel(application) {

    private val lessonModel = LessonModel(Database.getInstance(application).lessonDao)
    private val wordTranslationModel = TranslationModel(Database.getInstance(application).wordTranslationDao)

    private val lclApplication = application
    var lessonId: Long = -1L

    private val _listOfDirectories = MutableLiveData<Array<File>>()
    val listOfDirectories: LiveData<Array<File>>
        get() = _listOfDirectories

    private val _goToNextFile = MutableLiveData<String>()
    val goToNextFile: LiveData<String>
        get() = _goToNextFile

    private val _openChooseLessonDialog = MutableLiveData<String>()
    val openChooseLessonDialog: LiveData<String>
        get() = _openChooseLessonDialog

    private val _filePathForLoading = MutableLiveData<String>()
    val filePathForLoading: LiveData<String>
        get() = _filePathForLoading

    fun setDirectory(file: File?) {
        var f = file
        if (f == null) {
            f = Environment.getExternalStorageDirectory()
        }
        _listOfDirectories.value = f!!.listFiles()
    }

    fun onFileClick(file: File) {
        if (file.isDirectory){
            _goToNextFile.value = file.absolutePath
        } else {
           _openChooseLessonDialog.value = file.absolutePath
        }
    }

    fun startLoadingFile(filePath: String){
        _filePathForLoading.value = filePath
    }

    fun onFileClickComplete(){
        _goToNextFile.value = null
    }

    fun onFilePathForLoadingComplete(){
        _filePathForLoading.value = null
    }

    private suspend fun saveImportedTranslations(translationList: ArrayList<ImportedTranslation>) {

    }
}