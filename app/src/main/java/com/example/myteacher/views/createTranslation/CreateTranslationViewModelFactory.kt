package com.example.myteacher.views.createTranslation

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CreateTranslationViewModelFactory(private val lessonId: Long,
                                        private val application: Application
): ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateTranslationViewModel::class.java)) {
            return CreateTranslationViewModel(lessonId, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}