package com.example.myteacher.views.importTranslations.chooseLessonDialog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myteacher.dataClasses.lesson.lesson.Lesson
import com.example.myteacher.databinding.OverviewLessonListItemSimpleBinding
import com.example.myteacher.views.overview.LessonClickListener

class ChooseLessonDialogRecyclerAdapter(private val clickListener: LessonClickListener): ListAdapter<Lesson,
        ChooseLessonDialogRecyclerAdapter.ViewHolder>(LessonDifCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(clickListener, getItem(position))
    }


    class ViewHolder private constructor(val binding: OverviewLessonListItemSimpleBinding)
        : RecyclerView.ViewHolder(binding.root){

        fun bind(clickListener: LessonClickListener, item: Lesson){
            binding.lesson = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = OverviewLessonListItemSimpleBinding.inflate(layoutInflater,
                    parent, false)

                return ViewHolder(binding)
            }
        }

    }


}

class LessonDifCallback: DiffUtil.ItemCallback<Lesson>(){
    override fun areItemsTheSame(oldItem: Lesson, newItem: Lesson): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Lesson, newItem: Lesson): Boolean {
        return oldItem == newItem
    }

}