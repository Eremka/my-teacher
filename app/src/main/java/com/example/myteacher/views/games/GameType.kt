package com.example.myteacher.views.games

enum class GameType {
    NO_GAME, EASY_L1_TO_L2, EASY_L2_TO_L1, MEDIUM_L1_TO_L2, MEDIUM_L2_TO_L1, DIFFICULT
}