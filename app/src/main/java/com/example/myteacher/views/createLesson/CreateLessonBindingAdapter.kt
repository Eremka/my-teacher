package com.example.myteacher.views.createLesson

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener


class CreateLessonBindingAdapter {

    companion object {

        @BindingAdapter("languageAttrChanged")
        @JvmStatic fun setInverseBindingListener(view: Spinner, listener: InverseBindingListener){
            view.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    // Do nothing
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?,
                    position: Int, id: Long) {
                    listener.onChange()
                }

            }
        }

        @BindingAdapter ("language")
        @JvmStatic
        fun setLanguage(view: Spinner, newValue: String){
            if (!view.selectedItem.equals(newValue)){
                view.setSelection(view.selectedItemPosition)
            }
        }

        @InverseBindingAdapter(attribute = "language")
        @JvmStatic
        fun Spinner.getSelectedValue(): String{
            return selectedItem as String
        }

    }
}