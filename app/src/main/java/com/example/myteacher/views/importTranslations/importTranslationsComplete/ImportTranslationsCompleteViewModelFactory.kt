package com.example.myteacher.views.importTranslations.importTranslationsComplete

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ImportTranslationsCompleteViewModelFactory (
    private val path: String,
    private val databaseId: Long,
    private val application: Application) : ViewModelProvider.Factory {
        @Suppress("unchecked_cast")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ImportTranslationsCompleteViewModel::class.java)) {
                return ImportTranslationsCompleteViewModel(path, databaseId, application) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
}