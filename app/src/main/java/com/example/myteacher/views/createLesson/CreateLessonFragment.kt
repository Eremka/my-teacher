package com.example.myteacher.views.createLesson

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentCreateLessonBinding

class CreateLessonFragment: Fragment() {

    lateinit var viewModel: CreateLessonViewModel

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.saveLessonsMenu -> {
                viewModel.save()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.create_menu, menu)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentCreateLessonBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_create_lesson, container, false)

        setHasOptionsMenu(true)

        viewModel = ViewModelProviders.of(this)[CreateLessonViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.goBackToOverview.observe(this, Observer {
            if (it){
                activity?.onBackPressed()
                viewModel.goBackToOverviewComplete()
            }
        })

        return binding.root
    }
}