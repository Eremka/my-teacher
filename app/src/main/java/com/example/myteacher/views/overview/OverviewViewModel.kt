package com.example.myteacher.views.overview

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.lesson.lesson.LessonModel

class OverviewViewModel(
    application: Application
) : AndroidViewModel(application) {

    private val model =
        LessonModel(
            Database.getInstance(application).lessonDao
        )
    val allLessons = model.allLessons
    val numberOfLessons = model.numberOfLessons

    /**
     * Navigation to add a new lesson
     */
    private val _navigateToAddLesson = MutableLiveData<Boolean>()
    val navigateToAddLesson: LiveData<Boolean>
        get() = _navigateToAddLesson

    fun navigateToCreateBook() {
        _navigateToAddLesson.value = true
    }

    fun navigateToCreateBookComplete() {
        _navigateToAddLesson.value = false
    }

    /**
     * Navigation to the learning games
     */
    private val _navigateToLearningGames = MutableLiveData<Long>()
    val navigateToLearningGames: LiveData<Long>
        get() = _navigateToLearningGames

    fun navigateToLearningGames(id: Long) {
        _navigateToLearningGames.value = id
    }

    fun navigateToLearningGamesComplete() {
        _navigateToLearningGames.value = -1L
    }

    /**
     * Navigation to adding new translations
     */
    private val _navigateToTranslationsList = MutableLiveData<Long>()
    val navigateToTranslationsList: LiveData<Long>
        get() = _navigateToTranslationsList

    fun navigateToTranslationsList(id: Long) {
        _navigateToTranslationsList.value = id
    }

    fun navigateToTranslationsListComplete() {
        _navigateToTranslationsList.value = -1L
    }

    private val _navigateToImportTranslations = MutableLiveData<Long>()
    val navigateToImportTranslations: LiveData<Long>
        get() = _navigateToImportTranslations

    fun navigateToImportTranslations(id: Long){
        _navigateToImportTranslations.value = id
    }

    fun navigateToImportTranslationsComplete(){
        _navigateToImportTranslations.value = -1L
    }

}