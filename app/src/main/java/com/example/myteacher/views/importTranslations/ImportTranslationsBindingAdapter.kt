package com.example.myteacher.views.importTranslations

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.example.myteacher.R
import java.io.File

class ImportTranslationsBindingAdapter {

    companion object{
        @BindingAdapter("importTranslationsFileImage")
        @JvmStatic fun importTranslationsFileImage(view: ImageView, dir: File){
            if (dir.isDirectory){
                if (dir.listFiles().isEmpty()){
                    view.setImageDrawable(view.context.getDrawable(R.drawable.ic_folder_empty_58dp))
                } else {
                    view.setImageDrawable(view.context.getDrawable(R.drawable.ic_folder_full_58dp))
                }
            } else {
                when (dir.extension){
                    view.context.getString(R.string.csv) -> view.setImageDrawable(view.context.getDrawable(R.drawable.ic_csv_56dp))
                    view.context.getString(R.string.xls) -> view.setImageDrawable(view.context.getDrawable(R.drawable.ic_xls_56dp))
                    else -> view.setImageDrawable(view.context.getDrawable(R.drawable.ic_blank_file_56dp))
                }
            }
        }
    }
}