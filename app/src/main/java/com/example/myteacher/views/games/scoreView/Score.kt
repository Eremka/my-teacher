package com.example.myteacher.views.games.scoreView

import android.content.Context
import android.hardware.input.InputManager
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.myteacher.MainActivity

import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentScoreBinding
import com.example.myteacher.views.games.GameViewModel
import timber.log.Timber

class Score : Fragment() {

    private lateinit var viewModel: ScoreViewModel
    private lateinit var gameViewModel: GameViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentScoreBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_score, container, false)
        val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)

        viewModel = ViewModelProviders.of(this)[ScoreViewModel::class.java]
        gameViewModel = ViewModelProviders.of(activity!!)[GameViewModel::class.java]

        viewModel.numberOfWordsInLesson.observe(this, Observer {
            binding.scoreTranslationsAmountTv.text = getString(R.string.number_plus_words, it)
        })

        viewModel.lastLesson.observe(this, Observer {
            if (it != null){
                binding.scoreTotalTimeTv.text = getString(R.string.number_plus_min, it.duration)
                binding.scoreNewWordsTv.text = getString(R.string.number_plus_words, it.newWordsLearned)
                binding.scoreImprovementTv.text = "${it.improvementPercentage}%"
            }
        })

        viewModel.lessonComplete.observe(this, Observer {
            binding.scoreProgressInBarTv.text = "${it.toInt()}%"
            binding.scoreGeneralProgress.incrementProgressBy(it.toInt())
            binding.scoreProgressTv.text = "${it}%"
        })

        viewModel.wordsCorrectAtFirstTry.observe(this, Observer {
            binding.scoreFirstTryCorrectTv.text = getString(R.string.number_plus_words, it)
        })

        binding.viewModel = viewModel

        return binding.root
    }

}
