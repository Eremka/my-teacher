package com.example.myteacher.views.importTranslations.chooseLessonDialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentOverviewBinding
import com.example.myteacher.views.overview.LessonClickListener
import com.example.myteacher.views.overview.OverviewLessonRecyclerViewAdapter

class ChooseLessonDialog: DialogFragment() {

    internal lateinit var listener: ChooseLessonListener
    lateinit var viewModel: ChooseLessonViewModel

    interface ChooseLessonListener {
        fun onLessonClicked(id: Long)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = targetFragment as ChooseLessonListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException((targetFragment.toString() +
                    " must implement NoticeDialogListener"))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentOverviewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_overview,
            container, false)

        viewModel = ViewModelProviders.of(this)[ChooseLessonViewModel::class.java]

        val adapter = ChooseLessonDialogRecyclerAdapter(LessonClickListener { id ->
            listener.onLessonClicked(id)
        })

        binding.overviewFab.isVisible = false
        binding.overviewRecyclerView.adapter = adapter

        viewModel.allLessons.observe(this, Observer {
            if (it.isNotEmpty()){
                binding.overviewEmptyTextView.isVisible = false
                adapter.submitList(it)
            }
        })

        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }
}