package com.example.myteacher.views.createTranslation

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.lesson.lesson.LessonModel
import com.example.myteacher.dataClasses.translation.TranslationModel
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslation
import kotlinx.coroutines.*

class CreateTranslationViewModel(val databaseId: Long, application: Application) :
    AndroidViewModel(application) {

    private val lessonData =
        LessonModel(
            Database.getInstance(
                application
            ).lessonDao
        )
    private val translationData = TranslationModel(
        Database.getInstance(application).wordTranslationDao
    )

    var translation =
        WordTranslation(databaseId)
    val lesson = lessonData.getLessonWithId(databaseId)

    private val job = Job()
    private val uiScope = CoroutineScope(Dispatchers.IO + job)

    fun saveTranslation() {
        lesson.value!!.numberOfEntries++

        uiScope.launch {
            translationData.saveTranslation(translation)
            lessonData.updateLesson(lesson.value!!)
        }

        goToCreateTranslation()
        showSnackBar(lesson.value!!.numberOfEntries)
    }

    private val _goToCreateTranslation = MutableLiveData<Boolean>()
    val goToCreateTranslation: LiveData<Boolean>
        get() = _goToCreateTranslation

    private fun goToCreateTranslation() {
        _goToCreateTranslation.value = true
    }

    fun goToCreateTranslationComplete() {
        _goToCreateTranslation.value = false
    }

    private val _showSnackBar = MutableLiveData<Int>()
    val showSnackBar: LiveData<Int>
        get() = _showSnackBar

    private fun showSnackBar(numberOfEntries: Int) {
        _showSnackBar.value = numberOfEntries
    }

    fun showSnackBarComplete() {
        _showSnackBar.value = 0
    }


    override fun onCleared() {
        super.onCleared()
        uiScope.cancel()
    }
}