package com.example.myteacher.views.games.difficultGame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.myteacher.R
import com.example.myteacher.databinding.FragmentDifficultGameOneBinding
import com.example.myteacher.views.games.GameViewModel

class DifficultGameOne: Fragment() {

    private lateinit var gameViewModel: GameViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val binding: FragmentDifficultGameOneBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_difficult_game_one, container, false)

        gameViewModel = ViewModelProviders.of(activity!!)[GameViewModel::class.java]

        binding.viewModel = gameViewModel

        gameViewModel.startDifficultGame.observe(this, Observer {
            if (it){
                gameViewModel.gameStarted()
                this.findNavController().navigate(R.id.action_difficultGameOne_self)
            }
        })

        gameViewModel.gameFinished.observe(this, Observer {
            if (it){
                this.findNavController().navigate(R.id.action_difficultGameOne_to_score)
            }
        })

        gameViewModel.showAnswer.observe(this, Observer {
            if (it){
                binding.difficultGameOneAnswerTextView.visibility = View.VISIBLE
                binding.difficultGameOneTranslationEditText.visibility = View.GONE
            } else {
                binding.difficultGameOneAnswerTextView.visibility = View.GONE
                binding.difficultGameOneTranslationEditText.visibility = View.VISIBLE
            }
        })

        return binding.root
    }
}