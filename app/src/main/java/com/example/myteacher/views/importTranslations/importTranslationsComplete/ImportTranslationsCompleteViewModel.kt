package com.example.myteacher.views.importTranslations.importTranslationsComplete

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.myteacher.R
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.lesson.lesson.Lesson
import com.example.myteacher.dataClasses.lesson.lesson.LessonModel
import com.example.myteacher.dataClasses.translation.ImportedTranslation
import com.example.myteacher.dataClasses.translation.TranslationModel
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File

class ImportTranslationsCompleteViewModel(
    path: String,
    databaseId: Long,
    application: Application
) :
    AndroidViewModel(application) {

    private val translationModel =
        TranslationModel(Database.getInstance(application).wordTranslationDao)
    private val lessonModel = LessonModel(Database.getInstance(application).lessonDao)

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _goBackToTranslationList = MutableLiveData<Boolean>()
    val goBackToTranslationList: LiveData<Boolean>
        get() = _goBackToTranslationList

    private val _numberOfTranslationsImported = MutableLiveData<Int>()
    val numberOfTranslationsImported: LiveData<Int>
        get() = _numberOfTranslationsImported

    init {
        _isLoading.value = true
        val file = File(path)
        var translationsList: ImportedTranslation? = null

        viewModelScope.launch {
            when (file.extension) {
                application.getString(R.string.csv) -> translationsList =
                    importTranslationsCSV(file, databaseId)
                application.getString(R.string.xlsx) -> translationsList =
                    importTranslationsXLSX(file, databaseId)
            }

            countTranslations(translationsList)
            saveTranslations(translationsList)
            _isLoading.value = false
        }
    }

    private suspend fun saveTranslations(translationsList: ImportedTranslation?) {
        if (translationsList != null) {
            for (translation in translationsList.translationList) {
                Timber.d("Saving translation: ${translation.toString()}")
                translationModel.saveTranslation(translation)
                lessonModel.addTranslationToLesson(translation.databaseId)
            }
        }
    }

    private fun countTranslations(translationsList: ImportedTranslation?) {
        _numberOfTranslationsImported.value = translationsList?.translationList?.size
        Timber.d("Number of transactions imported: ${_numberOfTranslationsImported.value}")
    }

    fun goBackToTranslationList() {
        _goBackToTranslationList.value = true
    }

    fun goBackToTranslationListComplete() {
        _goBackToTranslationList.value = false
    }

}
