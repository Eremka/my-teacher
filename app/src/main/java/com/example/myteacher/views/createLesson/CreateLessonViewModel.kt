package com.example.myteacher.views.createLesson

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.myteacher.dataClasses.Database
import com.example.myteacher.dataClasses.lesson.lesson.Lesson
import com.example.myteacher.dataClasses.lesson.lesson.LessonModel
import kotlinx.coroutines.*


class CreateLessonViewModel(
    application: Application
) : AndroidViewModel(application) {

    private val lessonDatabase = Database.getInstance(application).lessonDao
    private val database =
        LessonModel(
            lessonDatabase
        )
    private val _goBackToOverview = MutableLiveData<Boolean>()
    val goBackToOverview: LiveData<Boolean>
        get() = _goBackToOverview

    var lesson = Lesson()

    val job = Job()
    val uiScope = CoroutineScope(Dispatchers.IO + job)

    fun save() {
        uiScope.launch {
            database.insertLesson(lesson)
        }

        _goBackToOverview.value = true
    }

    fun goBackToOverviewComplete(){
        _goBackToOverview.value = false
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}