package com.example.myteacher.views.translationList

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TranslationListViewModelFactory(
    private val lessonId: Long,
    private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TranslationListViewModel::class.java)) {
            return TranslationListViewModel(lessonId, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}