package com.example.myteacher.views.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var firebaseAuth : FirebaseAuth

    private val _isUserSignedIn = MutableLiveData<Boolean>()
    val isUserSignedIn: LiveData<Boolean>
        get() = _isUserSignedIn

    init {
        _isUserSignedIn.value = getCurrentUser() != null
    }

    private fun getCurrentUser(): FirebaseUser? {
        firebaseAuth = FirebaseAuth.getInstance()
        return firebaseAuth.currentUser
    }
}