package com.example.myteacher

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.myteacher.views.games.GameHostFragment
import com.example.myteacher.views.games.FinishGameDialog
import timber.log.Timber

class MainActivity : AppCompatActivity(), FinishGameDialog.FinishGameListener {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navHostFragment: NavHostFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl")
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl")
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl")

        navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.overviewFragment))
        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        val toolbar: Toolbar = findViewById(R.id.main_toolbar)
        setSupportActionBar(toolbar)
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)

        /**
         * Initializing the timber tree
         */
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(Timber.asTree())
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return if (navController.currentDestination!!.label == getString(R.string.game_host_fragment)) {
            val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
            val gameHostFragment = navHostFragment?.childFragmentManager!!.fragments[0] as GameHostFragment
            gameHostFragment.pauseTimer()

            val wantToFinishGameDialog = FinishGameDialog()
            wantToFinishGameDialog.show(supportFragmentManager, getString(R.string.finish_game_tag))
            false
        } else {
            NavigationUI.navigateUp(navController, appBarConfiguration)
        }
    }

    override fun onFinishGame(dialog: DialogFragment) {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        val gameHostFragment = navHostFragment?.childFragmentManager!!.fragments[0] as GameHostFragment
        gameHostFragment.finishGame()

        NavigationUI.navigateUp(navController, appBarConfiguration)
    }

    override fun onNotFinishGame(dialog: DialogFragment) {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        val gameHostFragment = navHostFragment?.childFragmentManager!!.fragments[0] as GameHostFragment
        gameHostFragment.continueTimer()

        dialog.dismiss()
    }
}
