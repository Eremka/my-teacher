package com.example.myteacher.dataClasses.lesson.completedLesson

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

private const val TABLE_NAME = "completed_lesson_table"

@Entity(tableName = TABLE_NAME)
class CompletedLesson (@PrimaryKey(autoGenerate = true) var id: Long = 0L,
                       @ColumnInfo(name = "lesson_id") var lessonId: Long,
                       @ColumnInfo(name = "time_started") var timeStarted: Long,
                       @ColumnInfo(name = "duration") var duration: String = "",
                       @ColumnInfo(name = "number_correct") var numberCorrect: Int = 0,
                       @ColumnInfo(name = "number_attempt") var numberAttempt: Int = 0,
                       @ColumnInfo(name = "new_words_learned") var newWordsLearned: Int = 0,
                       @ColumnInfo(name = "improvement_percentage") var improvementPercentage: Double = 0.0){

    override fun toString(): String {
        return "id: $id, lessonId $lessonId, timeStarted: $timeStarted, duration: $duration" +
                " number correct: $numberCorrect, number attempt: $numberAttempt" +
                " new words learned: $newWordsLearned, improvement percentage: $improvementPercentage"
    }
}