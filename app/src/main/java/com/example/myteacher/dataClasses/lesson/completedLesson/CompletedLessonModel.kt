package com.example.myteacher.dataClasses.lesson.completedLesson

class CompletedLessonModel(private val dataSource: CompletedLessonDao) {

    suspend fun insertLesson(lesson: CompletedLesson): Long{
        return dataSource.insert(lesson)
    }

    suspend fun updateLesson(lesson: CompletedLesson): Int{
        return dataSource.update(lesson)
    }

    suspend fun getLastTwoCompletedLessons(): List<CompletedLesson> {
        return dataSource.getLastTwoCompletedLessons()
    }

}