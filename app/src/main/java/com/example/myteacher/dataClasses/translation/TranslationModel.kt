package com.example.myteacher.dataClasses.translation

import androidx.lifecycle.LiveData
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslation
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslationDao

class TranslationModel(private val wordTranslations: WordTranslationDao) {

    suspend fun saveTranslation(translation: WordTranslation){
        wordTranslations.insert(translation)
    }

    suspend fun updateTranslation(translation: WordTranslation){
        wordTranslations.update(translation)
    }

    fun deleteTranslation(translation: WordTranslation){
        wordTranslations.delete(translation)
    }

    suspend fun getAllWordTranslationsAsList(databaseId: Long): List<WordTranslation>? {
        return wordTranslations.getAllWordTranslationsFromDatabase(databaseId)
    }

    fun getAllWordTranslationsInDatabase(databaseId: Long): LiveData<List<WordTranslation>>{
        return wordTranslations.getAllWordTranslationsFromDatabaseAsync(databaseId)
    }

    fun getWordTranslation(id: Long): LiveData<WordTranslation>{
        return wordTranslations.getWordTranslation(id)
    }

    fun getNumberOfTranslationsInLessonLiveData(lessonId: Long): LiveData<Int>{
        return wordTranslations.getNumberOfTranslationsInLessonLiveData(lessonId)
    }

    suspend fun getNumberOfTranslationsInLesson(lessonId: Long): Int{
        return wordTranslations.getNumberOfTranslationsInLesson(lessonId)
    }

    suspend fun getPercentageTranslationsLearned(lessonId: Long): Double{
        val wordsLearned = wordTranslations.getNumberTranslationsLearned(lessonId).toDouble()
        val wordsInDatabase = wordTranslations.getNumberOfTranslationsInDatabase(lessonId).toDouble()

        return (wordsLearned / wordsInDatabase) * 100
    }

}