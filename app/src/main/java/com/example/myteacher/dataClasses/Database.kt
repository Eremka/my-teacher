package com.example.myteacher.dataClasses

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myteacher.dataClasses.lesson.completedLesson.CompletedLesson
import com.example.myteacher.dataClasses.lesson.completedLesson.CompletedLessonDao
import com.example.myteacher.dataClasses.lesson.lesson.Lesson
import com.example.myteacher.dataClasses.lesson.lesson.LessonDao
import com.example.myteacher.dataClasses.translation.answer.Answer
import com.example.myteacher.dataClasses.translation.answer.AnswerDao
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslation
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslationDao

const val DATABASE_VERSION = 1

@androidx.room.Database(entities = [Lesson::class, WordTranslation::class,
                                    CompletedLesson::class, Answer::class],
                                    version = DATABASE_VERSION, exportSchema = false)
abstract class Database: RoomDatabase() {

    abstract val lessonDao: LessonDao
    abstract val wordTranslationDao: WordTranslationDao
    abstract val completedLessonDao: CompletedLessonDao
    abstract val answerDao: AnswerDao

    /**
     * Define a companion object, this allows us to add functions on the SleepDatabase class.
     *
     * For example, clients can call `SleepDatabase.getInstance(context)` to instantiate
     * a new SleepDatabase.
     */
    companion object{
        @Volatile
        private var INSTANCE: Database? = null

        /**
         * INSTANCE will keep a reference to any database returned via getInstance.
         *
         * This will help us avoid repeatedly initializing the database, which is expensive.
         *
         *  The value of a volatile variable will never be cached, and all writes and
         *  reads will be done to and from the main memory. It means that changes made by one
         *  thread to shared data are visible to other threads.
         */
        fun getInstance(context: Context): Database {

            // Multiple threads can ask for the database at the same time, ensure we only initialize
            // it once by using synchronized. Only one thread may enter a synchronized block at a
            // time.
            synchronized(this){
                var instance = INSTANCE

                if (instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        Database::class.java,
                        "lessons"
                        ).fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}