package com.example.myteacher.dataClasses.lesson.lesson

import androidx.lifecycle.LiveData
import androidx.room.*

private const val getNumberOfLessonsWithNameQuery = "SELECT * FROM lesson_table WHERE name LIKE :name LIMIT 1"
private const val getAllLessonsQuery = "SELECT * FROM lesson_table ORDER BY id DESC"
private const val getLastLessonQuery = "SELECT * FROM lesson_table ORDER BY id DESC LIMIT 1"
private const val getLessonWithIdQuery = "SELECT * FROM lesson_table WHERE id LIKE :id"
private const val getNumberOfLessonsQuery = "SELECT COUNT (*) FROM lesson_table"
private const val deleteAllQuery = "DELETE FROM lesson_table"
private const val addTranslationToLesson = "UPDATE lesson_table Set number_of_entries = number_of_entries + 1 WHERE id LIKE :id"

@Dao
interface LessonDao {

    @Insert
    fun insert(lesson: Lesson): Long

    @Update
    suspend fun update(lesson: Lesson)

    @Delete
    fun delete(lesson: Lesson)

    @Query(getAllLessonsQuery)
    fun getAllLessons(): LiveData<List<Lesson>>

    @Query(getLastLessonQuery)
    fun getLastLesson(): Lesson

    @Query(getLessonWithIdQuery)
    fun getLessonWithId(id: Long):LiveData<Lesson>

    @Query(getLessonWithIdQuery)
    suspend fun getLessonWithIdAsync(id: Long):Lesson

    @Query(getNumberOfLessonsQuery)
    fun getNumberOfLessons(): LiveData<Int>

    @Query(getNumberOfLessonsWithNameQuery)
    suspend fun getNumberOfLessonsWithName(name: String): Lesson?

    @Query(deleteAllQuery)
    fun deleteAllLessons()

    @Query(addTranslationToLesson)
    suspend fun addTranslationToLesson(id: Long)
}