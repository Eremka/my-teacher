package com.example.myteacher.dataClasses.translation.answer

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

private const val TABLE_NAME = "answer_table"

@Entity(tableName = TABLE_NAME)
class Answer(@PrimaryKey(autoGenerate = true) var id: Long = 0L,
             @ColumnInfo(name = "completed_lesson_id") var completedLessonId: Long = 0L,
             @ColumnInfo(name = "translation_id") val translationId: Long,
             @ColumnInfo(name = "is_correct") val isCorrect: Boolean,
             @ColumnInfo(name = "time") val time: Long,
             @ColumnInfo(name = "is_newly_learned") val isNewlyLearned: Boolean = false) {

    override fun toString(): String {
        return "id: $id, completed lesson id: $completedLessonId, translation id: $translationId, " +
                "correct: $isCorrect, time: $time, newly learned: $isNewlyLearned"
    }
}