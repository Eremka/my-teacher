package com.example.myteacher.dataClasses.translation.answer

class AnswerModel(private val dataSource: AnswerDao) {

    suspend fun insertAnswer(answer: Answer){
        dataSource.insert(answer)
    }

    suspend fun updateAnswer(answer: Answer){
        dataSource.update(answer)
    }

    suspend fun getExistingAnswerCount(translationId: Long, completedLessonId: Long): Int{
        return dataSource.getSimilarAnswerCount(translationId, completedLessonId)
    }

    suspend fun getPercentageCorrectAtFirstTry(completedLessonId: Long): Double{
        val totalAnswers = dataSource.getNumberOfAnswers(completedLessonId).toDouble()
        val totalAnswersCorrectFirstTry = dataSource.getNumberOfAnswersCorrectAtFirstTry(completedLessonId).toDouble()
        return (totalAnswersCorrectFirstTry / totalAnswers) * 100
    }

    suspend fun getAnswersCorrectAtFirstTry(completedLessonId: Long): Int{
        return dataSource.getNumberOfAnswersCorrectAtFirstTry(completedLessonId)
    }
}