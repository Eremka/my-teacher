package com.example.myteacher.dataClasses.translation.wordTranslation

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface WordTranslationDao {

    @Insert
    suspend fun insert(wordTranslation: WordTranslation)

    @Update
    suspend fun update(wordTranslation: WordTranslation)

    @Delete
    fun delete(wordTranslation: WordTranslation)

    @Query("SELECT * FROM word_translation_table WHERE ID LIKE :id")
    fun getWordTranslation(id: Long): LiveData<WordTranslation>

    @Query("SELECT * FROM word_translation_table ORDER BY id ASC")
    fun getAllWordTranslations(): LiveData<List<WordTranslation>>

    @Query("SELECT * FROM word_translation_table WHERE DATABASE_ID LIKE :databaseId ORDER BY id ASC")
    suspend fun getAllWordTranslationsFromDatabase(databaseId: Long): List<WordTranslation>

    @Query("SELECT * FROM word_translation_table WHERE DATABASE_ID LIKE :databaseId ORDER BY id ASC")
    fun getAllWordTranslationsFromDatabaseAsync(databaseId: Long): LiveData<List<WordTranslation>>

    @Query("SELECT * FROM word_translation_table ORDER BY id ASC LIMIT 1")
    fun getLastWordTranslation(): LiveData<WordTranslation>

    @Query("SELECT COUNT (*) FROM word_translation_table WHERE DATABASE_ID LIKE :databaseId")
    fun getNumberOfTranslationsInLessonLiveData(databaseId: Long): LiveData<Int>

    @Query("SELECT COUNT (*) FROM word_translation_table WHERE DATABASE_ID LIKE :databaseId")
    suspend fun getNumberOfTranslationsInLesson(databaseId: Long): Int

    @Query("SELECT * FROM word_translation_table WHERE DATABASE_ID LIKE :databaseId AND DATABASE_ID NOT IN (:list) ORDER BY RANDOM() LIMIT 1")
    fun getRandomTranslation(databaseId: Long, list: List<Long>): LiveData<WordTranslation>

    @Query("SELECT COUNT(*) FROM word_translation_table WHERE database_id LIKE :databaseId AND level_three > 2")
    suspend fun getNumberTranslationsLearned(databaseId: Long): Int

    @Query("SELECT COUNT(*) FROM word_translation_table WHERE database_id LIKE :databaseId")
    suspend fun getNumberOfTranslationsInDatabase(databaseId: Long): Int

    @Query("DELETE FROM word_translation_table")
    fun deleteAll()
}