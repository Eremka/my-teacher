package com.example.myteacher.dataClasses.translation

import androidx.databinding.BaseObservable
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

const val LEVEL_ONE = "level_one"
const val LEVEL_TWO = "level_two"
const val LEVEL_THREE = "level_three"
const val DATABASE_ID = "database_id"
const val NUMBER_ATTEMPTED = "number_attempted"
const val NUMBER_CORRECT = "number_correct"

open class Translation(@PrimaryKey(autoGenerate = true)var id: Long = 0L,
                       @ColumnInfo(name = NUMBER_ATTEMPTED) var attempts: Int = 0,
                       @ColumnInfo(name = NUMBER_CORRECT) var correct: Int = 0,
                       @ColumnInfo(name = LEVEL_ONE) var levelOne: Int = 0,
                       @ColumnInfo(name = LEVEL_TWO) var levelTwo: Int = 0,
                       @ColumnInfo(name = LEVEL_THREE) var levelThree: Int = 0,
                       @ColumnInfo(name = DATABASE_ID) var databaseId: Long): BaseObservable() {

}