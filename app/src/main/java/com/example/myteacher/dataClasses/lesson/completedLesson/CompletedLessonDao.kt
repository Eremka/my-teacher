package com.example.myteacher.dataClasses.lesson.completedLesson

import androidx.room.*

@Dao
interface CompletedLessonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(completedLesson: CompletedLesson): Long

    @Update
    suspend fun update(completedLesson: CompletedLesson): Int

    @Delete
    fun delete(completedLesson: CompletedLesson)

    @Query("SELECT * FROM completed_lesson_table ORDER BY time_started DESC LIMIT 2")
    suspend fun getLastTwoCompletedLessons(): List<CompletedLesson>
}