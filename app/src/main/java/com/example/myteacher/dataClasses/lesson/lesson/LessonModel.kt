package com.example.myteacher.dataClasses.lesson.lesson

import androidx.lifecycle.LiveData
import com.example.myteacher.dataClasses.lesson.lesson.Lesson
import com.example.myteacher.dataClasses.lesson.lesson.LessonDao
import timber.log.Timber

class LessonModel(private val dataSource: LessonDao) {

    val allLessons: LiveData<List<Lesson>> = dataSource.getAllLessons()
    val numberOfLessons: LiveData<Int> = dataSource.getNumberOfLessons()

    fun getLessonWithId(id: Long): LiveData<Lesson>{
        return dataSource.getLessonWithId(id)
    }

    suspend fun getLessonWithName(name: String): Lesson?{
        return dataSource.getNumberOfLessonsWithName(name)
    }

    fun insertLesson(lesson: Lesson): Long{
        return dataSource.insert(lesson)
    }

    suspend fun updateLesson(lesson: Lesson){
        dataSource.update(lesson)
    }

    suspend fun addTranslationToLesson(lessonId: Long){
        dataSource.addTranslationToLesson(lessonId)
    }
}