@file:Suppress("RedundantGetter")

package com.example.myteacher.dataClasses.translation.wordTranslation

import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.room.ColumnInfo
import androidx.room.Entity
import com.example.myteacher.dataClasses.translation.Translation

private const val TABLE_NAME = "word_translation_table"
const val LANGUAGE_ONE = "language_one"
const val LANGUAGE_TWO = "language_two"
const val EXAMPLE_SENTENCE = "example_sentence"
const val EXAMPLE_SENTENCE_ANSWER = "example_sentence_answer"

@Entity(tableName = TABLE_NAME)
data class WordTranslation(val database: Long) : Translation(databaseId = database) {

    @ColumnInfo(name = LANGUAGE_ONE) var language1 = ""
        @Bindable get() = field
        @Bindable set(value) {
            field = value
            notifyPropertyChanged(BR.language1)
        }

    @ColumnInfo(name = LANGUAGE_TWO) var language2 = ""
    @Bindable get() = field
    @Bindable set(value)
    {
        field = value
        notifyPropertyChanged(BR.language2)
    }

    @ColumnInfo(name = EXAMPLE_SENTENCE )var exampleSentence: String? = null
        @Bindable get() = field
        @Bindable set(value) {
            field = value
            notifyPropertyChanged(BR.exampleSentence)
        }

    @ColumnInfo(name = EXAMPLE_SENTENCE_ANSWER) var exampleSentenceAnswer: String? = null
        @Bindable get() = field
        @Bindable set(value) {
            field = value
            notifyPropertyChanged(BR.exampleSentenceAnswer)
        }

    override fun toString(): String {
        return "id: $id, database id: $databaseId, attempts: $attempts, correct: $correct, " +
                "level 1: $levelOne, level 2: $levelTwo, level 3: $levelThree, " +
                "language 1: $language1, language 2: $language2"
    }
}