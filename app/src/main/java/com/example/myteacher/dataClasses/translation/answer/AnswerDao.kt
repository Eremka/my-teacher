package com.example.myteacher.dataClasses.translation.answer

import androidx.room.*

@Dao
interface AnswerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(answer: Answer): Long

    @Update
    suspend fun update(answer: Answer): Int

    @Delete
    fun delete(answer: Answer)

    @Query("SELECT COUNT(*) FROM answer_table WHERE completed_lesson_id LIKE :completedLessonId AND translation_Id LIKE :translationId")
    suspend fun getSimilarAnswerCount(translationId: Long, completedLessonId: Long): Int

    @Query("SELECT COUNT(*) FROM answer_table WHERE completed_lesson_id LIKE :completedLessonId")
    suspend fun getNumberOfAnswers(completedLessonId: Long): Int

    @Query("SELECT COUNT(*) FROM answer_table WHERE completed_lesson_id LIKE :completedLessonId AND is_newly_learned = 1")
    suspend fun getNumberOfAnswersCorrectAtFirstTry(completedLessonId: Long): Int
}