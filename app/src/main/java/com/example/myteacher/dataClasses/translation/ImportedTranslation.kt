package com.example.myteacher.dataClasses.translation

import com.example.myteacher.dataClasses.lesson.lesson.Lesson
import com.example.myteacher.dataClasses.translation.wordTranslation.WordTranslation

class ImportedTranslation(var lesson: Lesson,
                          var translationList: ArrayList<WordTranslation>) {
}