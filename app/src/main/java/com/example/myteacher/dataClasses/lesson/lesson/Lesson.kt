@file:Suppress("RedundantGetter")

package com.example.myteacher.dataClasses.lesson.lesson

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

private const val TABLE_NAME = "lesson_table"

@Entity(tableName = TABLE_NAME)
data class Lesson(
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    @ColumnInfo(name = "percentage_correct") var percentageCorrect: Double = 0.0,
    @ColumnInfo(name = "number_of_entries") var numberOfEntries: Int = 0,
    @ColumnInfo(name = "is_shares") var isShared: Boolean = false
) : BaseObservable() {

//    @get:Bindable var name by Delegates.observable(_name){ _, _, _ ->
//        notifyPropertyChanged(BR.name)
//    }

    @ColumnInfo(name = "name")
    var name: String = ""
        @Bindable get() = field
        @Bindable set(value) {
            field = value
            notifyPropertyChanged(BR.name)
        }

    @ColumnInfo(name = "language_one")
    var language1: String = ""
        @Bindable get() = field
        @Bindable set(value) {
            field = value
            notifyPropertyChanged(BR.language1)
        }

    @ColumnInfo(name = "language_two")
    var language2: String = ""
        @Bindable get() = field
        @Bindable set(value) {
            field = value
            notifyPropertyChanged(BR.language2)
        }

    @ColumnInfo(name = "creator")
    var creator: String = ""
        @Bindable get() = field
        @Bindable set(value) {
            field = value
            notifyPropertyChanged(BR.creator)
        }
}